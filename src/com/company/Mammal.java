package com.company;

public class  Mammal extends Creature {

    private String mammalName;
    public String getMammalName(){ return  mammalName;}

    private int mammalAge;
    public int getMammalAge(){ return mammalAge;}

    private boolean mammalExistance;
    public boolean getMammalExistance(){ return mammalExistance;}

    protected int numberOfLegs;
    public int getNumberOfLegs(){ return numberOfLegs;}

    public Mammal(){}

    public Mammal(String mammalName, int mammalAge, boolean mammalExistance, int numberOfLegs){

        this.mammalName = mammalName;
        this.mammalExistance = mammalExistance;
        this.mammalAge = mammalAge;
        this.numberOfLegs = numberOfLegs;
    }

        public void walk(){
        if(numberOfLegs >= 2){
            System.out.println("The mammal can walk");
        }
    }

}
