package com.company;

public class Creature extends Cell{

    private String creatureName;
    public String getCreatureName(){ return creatureName;}

    private int creatureAge;
    public int getCreatureAge(){return creatureAge;}

    private boolean creatureExistance;
    public boolean getCreatureExistance(){return creatureExistance;}

    protected int numberOfLegs;
    public int getNumberOfLegs(){return numberOfLegs;}

    public Creature(){}

    public Creature(String creatureName, int creatureAge, boolean creatureExistance, int numberOfLegs){

        this.creatureExistance = creatureExistance;
        this.creatureName = creatureName;
        this.creatureAge = creatureAge;
        this.numberOfLegs = numberOfLegs;
    }

    private boolean life(boolean creatureExistance){
        return creatureExistance;
    }

    public void walk(){
      if(numberOfLegs >=2){
          System.out.println("Creature can walk");
      }
    }
    public void run(){
        System.out.println("Also it can run");
    }
}
