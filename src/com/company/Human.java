package com.company;

public class Human extends Mammal {

    private String name;
    public String getName(){ return name;}

    private int age;
    public int getAge(){ return age;}

    private String sex;
    public String getSex(){ return sex;}

    private int height;
    public int getHeight(){return height;}

    private int weight;
    public int getWeight(){return weight;}

    private String eyeColor;
    public String getEyeColor(){ return eyeColor;}

    private String hairColor;
    public String getHairColor(){ return hairColor;}

    public Human(){}

    public Human(String name, int age, String sex, int height, int weight, String eyeColor, String hairColor){

        this.name = name;
        this.age = age;
        this.sex = sex;
        this.height = height;
        this.weight = weight;
        this.eyeColor = eyeColor;
        this.hairColor = hairColor;
    }

    public void displayName(){
        System.out.printf("My name is %s", name);
    }

    enum Job{

        GUARD,
        BLACKSMITH,
        FARMER,
        MASON,
        CARPENTER,
        HEARTH_CEEPER,
        MOTHER
    }
}
