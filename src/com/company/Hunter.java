package com.company;

public class Hunter implements Fisher, Predator{

    private String hunterName;
    public String getHunterName(){ return hunterName;}

    private int hunterAge;
    public int getHunterAge(){ return hunterAge;}

    private String hunterSex;
    public String getHunterSex(){ return hunterSex;}

    private int hunterHeight;
    public int getHunterHeight(){return hunterHeight;}

    private int hunterWeight;
    public int getHunterWeight(){return hunterWeight;}

    private String hunterEyeColor;
    public String getHunterEyeColor(){ return hunterEyeColor;}

    private String hunterHairColor;
    public String getHunterHairColor(){ return hunterHairColor;}

    public Hunter(){}

    public Hunter(String name, int age, String sex, int height, int weight, String eyeColor, String hairColor){

        this.hunterName = name;
        this.hunterAge = age;
        this.hunterSex = sex;
        this.hunterHeight = height;
        this.hunterWeight = weight;
        this.hunterEyeColor = eyeColor;
        this.hunterHairColor = hairColor;
    }

    public void hunting(){
        System.out.println("This thing could kill and eat another animals");
    }

}
