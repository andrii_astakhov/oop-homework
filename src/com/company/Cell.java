package com.company;

class Cell extends Thing{

    private boolean cellExistance;
    public boolean getCellExistance(){return cellExistance;}

    private int count;
    public int getCount(){return count;}

    public Cell(){}

    public Cell(int count){
        this.count = count;
    }

    public Cell(boolean cellExistance){
        this.cellExistance = cellExistance;
    }

    public Cell(int count, boolean cellExistance){
        this.cellExistance = cellExistance;
        this.count = count;
    }

    protected void alive(){
        if(cellExistance && count >= 2){
            System.out.println("It is posible to create life!");
        }
    }
}
