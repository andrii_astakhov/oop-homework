package com.company;

public final class Dinosaur extends Creature{
    private String dinosaurName;
    public String getDinosaurName(){ return dinosaurName;}

    private int dinosaurAge;
    public int getDinosaurAge(){return dinosaurAge;}

    private boolean dinosaurExistance;
    public boolean getDinosaurExistance(){return dinosaurExistance;}

    protected int numberOfLegs;
    public int getNumberOfLegs(){return numberOfLegs;}

    public Dinosaur(){}

    public Dinosaur(String dinosaurName, int dinosaurAge, boolean dinosaurExistance, int numberOfLegs){

        this.dinosaurExistance = dinosaurExistance;
        this.dinosaurName = dinosaurName;
        this.dinosaurAge = dinosaurAge;
        this.numberOfLegs = numberOfLegs;
    }

    private boolean life(boolean dinosaurExistance){
        return dinosaurExistance;
    }

    public void walk(){
        if(numberOfLegs >=2){
            System.out.println("Creature can walk");
        }
    }

    public void run(){
        super.run();
    }

    public class TRex{

        private int tRexAge;
        public int gettRexAge(){return tRexAge;}

        private int tRexNumberOfLegs;
        public int gettRexNumberOfLegs(){return tRexNumberOfLegs;}

        public void walk(){
            if(tRexNumberOfLegs>=2){
                System.out.println("T-Rex can walk");
            }
        }

        public TRex(){}
    }
}
